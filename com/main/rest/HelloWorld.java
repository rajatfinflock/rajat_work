package com.main.rest;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.ib.client.ExecutionFilter;
import com.ib.client.Order;
import com.mysql.jdbc.Connection;

import Test2.DataSourceClass;
import Test2.JSONArrayExecutionPOJO;
import Test2.JSONArrayPojo;
import Test2.MyEwrapperInterface;
import Test2.OrderId;
import Test2.TwsTest2;


import samples.testbed.contracts.ContractSamples;
import samples.testbed.orders.OrderSamples;

@Path("/Order")
public class HelloWorld {

static	EClientSocket client = null;
	 //-----------------------------------------------------------market----------------
	 @POST
	 @Path("/buy/{userid}/{symbol}/{quantity}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public static String test5(@PathParam("userid") String userId,@PathParam("symbol") String symbol,@PathParam("quantity") String quantity) {
		  ResultSet rs;int a = 0;double b = 0;String c=null,d=null;
		  
		System.out.println("buy method  -----------------------------------------------------  ");
			if(client==null) {
				 TwsTest2 obj=new TwsTest2();
				client	=   obj.mainMethod();
			
			}
		//	 System.out.println("------------connection object-----"+client);
			 	
			
		 int qnty=Integer.parseInt(quantity);
		
		 //getting next id
		 client.reqIds(-1);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int id=OrderId.getOId();
		
		id=id+1;
	//setting order property
		   Order order = new Order();
	       order.action("BUY");
	       order.orderType("MKT");
	       order.totalQuantity(qnty);
	       order.transmit(true);
	       
		      Contract cnt=new Contract();
		      cnt.currency("USD");
		       cnt.symbol(symbol);
		       cnt.exchange("SMART");
		       cnt.secType("STK");
		  	 System.out.println("------------place order-----"+client);
client.placeOrder(id, cnt, order);




//----------------------
try {
	

java.sql.Connection con=		DataSourceClass.getDataSource().getConnection();
	 
java.sql.PreparedStatement pstmt = con.prepareStatement(
		"insert into orderdes(userId,orderid,symbol,quantity,orderActivity) values (?,?,?,?,?)"); 
			
			  pstmt.setString(1, userId);
			  pstmt.setInt(2, id);
			
			  pstmt.setString(3, symbol);
			  pstmt.setInt(4, qnty);
			  pstmt.setString(5, "sell");
			 pstmt.executeUpdate();

	     

   }
catch(Exception e) {System.out.println("database error from rest class sell--"+e);}


//-----------
//client.reqExecutions(10001, new ExecutionFilter());
	  return "{userId:"+userId+",orderId:"+id+",Quantity:"+quantity+",Symbol:"+symbol+",type:"+"buy"+"}";
	 }
	 
	 
	 
	 
	 
	 @POST
	 @Path("/sell/{userId}/{symbol}/{quantity}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public static String test6(@PathParam("userId") String userId,@PathParam("symbol") String symbol,@PathParam("quantity") String quantity) {
		 ResultSet rs;int a = 0;double b = 0;String c=null,d=null;  int id = 0;
		
System.out.println("sell method-------------------------");
			if(client==null) {
				 TwsTest2 obj=new TwsTest2();
				client	=   obj.mainMethod();
			
			}
			
		 int qnty=Integer.parseInt(quantity);
		 
	 Order order = new Order();
	       order.action("SELL");
	       order.orderType("MKT");
	       order.totalQuantity(qnty);
	       order.transmit(true);
	       
	      Contract cnt=new Contract();
	      cnt.currency("USD");
	       cnt.symbol(symbol);
	       cnt.exchange("SMART");
	       cnt.secType("STK");
	       
	       client.reqIds(-1);
	       try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  
	       id=OrderId.getOId();
	       id=id+1;
	 System.out.println("sell  ----order placed---------"+id);
	client.placeOrder(id, cnt, order);
	
	
	 //----------------------
			try {
				
			
	java.sql.Connection con=		DataSourceClass.getDataSource().getConnection();
				 
				  java.sql.PreparedStatement pstmt = con.prepareStatement(
			"insert into orderdes(userId,orderid,symbol,quantity,orderActivity) values (?,?,?,?,?)"); 
				
				  pstmt.setString(1, userId);
				  pstmt.setInt(2, id);
				
				  pstmt.setString(3, symbol);
				  pstmt.setInt(4, qnty);
				  pstmt.setString(5, "sell");
				 pstmt.executeUpdate();

				     
			
			   }
			catch(Exception e) {System.out.println("database error from rest class sell---"+e);}
			
			
			//-----------
//client.reqExecutions(10001, new ExecutionFilter());
	//ExecutionFilter( int p_clientId, String p_acctCode, String p_time,	String p_symbol, String p_secType, String p_exchange, String p_side) {
return "{userId:"+userId+",orderId:"+id+",Quantity:"+quantity+",Symbol:"+symbol+",type:"+"sell"+"}";
			
			
			
			
	 }
	 
	 
		@GET
		 @Path("/orderStatus/{id}")
		 @Produces(MediaType.APPLICATION_JSON)
		 public String test9(@PathParam("id") String id) {
			  JSONArray ja=new JSONArray();
			 JSONArrayPojo.setJa(ja);
			System.out.println("---------------------------------------oprn order method");
			  client.reqOpenOrders();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			  
			 ja= JSONArrayPojo.getJa();
			return ja.toString();
		 }  
	 
		@GET
		 @Path("/orderExecution/{id}")
		 @Produces(MediaType.APPLICATION_JSON)
		 public String test10(@PathParam("id") String id) {
			  JSONArray ja=new JSONArray();
			  JSONArrayExecutionPOJO.setJa(ja);
			System.out.println("---------------------------------------oprn order execution method");
			client.reqExecutions(10001, new ExecutionFilter());
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			  
			 ja= JSONArrayExecutionPOJO.getJa();
			return ja.toString();
		 }  
	 
	 
	
	 

}


	 
